<?php
session_start();
//diisi array kosong, supaya yakin sessionnya hilang
$_SESSION = [];
session_unset();
// akhiri sesi
session_destroy();
// jika sesi berakhir
header("location:login.php?status=logout");
exit;
