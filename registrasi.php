<?php
include 'config/koneksi.php';

function registrasi($data)
{ //menerima inputan data yg dikirim dari $post, ditangkap kesini
   global $koneksi; //ambil koneksi paling atas hlm ini

   $username = strtolower(stripslashes($data["username"])); //ambil data dari $_post, diambil olh var $data
   $nama = strtolower(stripslashes($data["nama"]));
   $password = mysqli_real_escape_string($koneksi, $data["password"]); //ada 2 parameter
   $password2 = mysqli_real_escape_string($koneksi, $data["password2"]);

   // cek username sudah ada atau belum
   $result = mysqli_query($koneksi, "SELECT Username FROM login WHERE Username = '$username'");
   if (mysqli_fetch_assoc($result)) {
      echo "<script>
			alert('Username Telah Terdaftar!');
			</script>";
      return false;   //berhentikan functionnya, supaya insert gagal dan yg bawah tidak dijalankan
   }

   // cek konfirmasi password
   if ($password !== $password2) {
      echo "<script>
				alert('Password Tidak Sesuai!');
			</script>";

      return false; //berhentikan function, supaya masuk ke else di registrasi
   }
   // enkripsi password
   $password = md5($password);

   // tambahkan userbaru ke database
   mysqli_query($koneksi, "INSERT INTO login VALUES('', '$username', '$password', '$nama')");
   return mysqli_affected_rows($koneksi);
}

if (isset($_POST["register"])) {

   if (registrasi($_POST) > 0) {
      echo "<script>
            location.href='login.php';
				alert ('User Baru Berhasil Ditambahkan!');
		  	  </script>";
   } else {
      echo mysqli_error($koneksi);
   }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="description" content="">
   <meta name="author" content="">
   <link rel="icon" href="img/buku.png" type="image/ico">
   <style>
      body {
         background: #007bff;
         background: linear-gradient(to right, #0062E6, #33AEFF);
      }
   </style>
   <title>Registrasi | R6C Library</title>

   <!-- Custom fonts for this template-->
   <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
   <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

   <!-- Custom styles for this template-->
   <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body>

   <div class="container">

      <!-- Outer Row -->
      <div class="row justify-content-center">

         <div class="col-xl-12 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
               <div class="card-body p-0">
                  <!-- Nested Row within Card Body -->
                  <div class="row">
                     <div class="col-lg-6 ">
                        <div class="p-5">
                           <div class="text-center">
                              <h1 class="h4 text-gray-900 mb-4"> <b>FORM REGISTRASI </b> <br> <b class="text-primary">R6C LIBRARY</b></h1>
                           </div>
                           <form class="user" action="" method="POST">
                              <div class="form-group">
                                 <input type="text" name="username" id="username" class="form-control form-control-user" placeholder="Username" required>
                              </div>
                              <div class="form-group">
                                 <input type="text" name="nama" id="nama" class="form-control form-control-user" placeholder="Name" required>
                              </div>
                              <div class="form-group">
                                 <input type="password" name="password" id="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password" required>
                              </div>
                              <div class="form-group">
                                 <input type="password" name="password2" id="password2" class="form-control form-control-user" id="exampleInputPassword" placeholder="Confirm Password" required>
                              </div>
                              <button type="submit" name="register" class="btn btn-primary btn-user btn-block">
                                 <b>REGISTRASI</b>
                              </button>
                           </form>
                           <hr>
                           <div class="text-left">
                              <p>Sudah Punya Akun?
                                 <u><a class="font-weight-normal" href="login.php">Kembali ke Halaman Login</a></u>
                              </p>
                           </div>

                        </div>
                     </div>
                     <div class="col-lg-6 d-none d-lg-block border-left-primary">
                        <img src="img/register.jpg" alt="">
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Bootstrap core JavaScript-->
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

   <!-- Core plugin JavaScript-->
   <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

   <!-- Custom scripts for all pages-->
   <script src="js/sb-admin-2.min.js"></script>

</body>

</html>