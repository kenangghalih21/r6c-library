<?php

include 'config/koneksi.php';
if (isset($_POST['ubah'])) {
   $kd = $_POST['a'];
   $judul = $_POST['b'];
   $kt = $_POST['c'];
   $pengarang = $_POST['d'];
   $penerbit = $_POST['e'];
   $thn = $_POST['f'];
   $sinopsis = $_POST['g'];


   $sql = mysqli_query($koneksi, "UPDATE buku SET
         Judul='$judul',
         Kategori='$kt',
         Pengarang='$pengarang',
         Penerbit='$penerbit',
         Thn='$thn',
         Sinopsis='$sinopsis' 
      WHERE KodeBuku='$kd'");
   if ($sql) {
      echo "<script>location.href='index.php?page=view-buku';alert('Berhasil Melakukan Perubahan');</script>";
   }
}

$kd = $_GET['kd'];
$tampil = mysqli_query($koneksi, "SELECT * FROM buku WHERE KodeBuku = '$kd'");
while ($data = mysqli_fetch_array($tampil)) { ?>

   <div class="row">
      <div class="col-md-12">
         <div class="card shadow mb-4">
            <div class="card-header py-3 d-sm-flex align-items-center justify-content-between mb-4">
               <h6 class="m-0 font-weight-bold text-primary">Ubah Data Buku</h6>
               <a href="index.php?page=view-buku" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-eye fa-sm text-white-50"></i> Tampil Data</a>
            </div>

            <form action="" method="POST">
               <div class="card-body col-md-6">
                  <div class="form-group">
                     <label>Kode Buku</label><br>
                     <h4><?= $data['KodeBuku'] ?></h4>
                     <input type="hidden" name="a" class="form-control" value="<?= $data['KodeBuku'] ?>" placeholder="Enter Kode Buku" required>
                  </div>
                  <div class="form-group">
                     <label>Judul Buku</label>
                     <input type="text" name="b" class="form-control" value="<?= $data['Judul'] ?>" placeholder="Enter Judul Buku" required>
                  </div>
                  <div class="form-group">
                     <label>Kategori Buku</label>
                     <select name="c" class="form-control">
                        <option selected disabled value="0">-Pilih Kategori-</option>
                        <?php
                        $dataKategori = mysqli_query($koneksi, "SELECT * FROM kategori");
                        while ($row = mysqli_fetch_array($dataKategori)) { ?>
                           <option value="<?= $row['IDKategori'] ?>" <?php if ($data['Kategori'] == $row['IDKategori']) {
                                                                        echo "selected";
                                                                     } ?>><?= $row['NmKategori'] ?></option>
                        <?php } ?>
                     </select>
                  </div>
                  <div class="form-group">
                     <label>Pengarang</label>
                     <input type="text" name="d" class="form-control" value="<?= $data['Pengarang'] ?>" placeholder="Enter Pengarang" required>
                  </div>
                  <div class="form-group">
                     <label>Penerbit</label>
                     <input type="text" name="e" class="form-control" value="<?= $data['Penerbit'] ?>" placeholder="Enter Penerbit" required>
                  </div>
                  <div class="form-group">
                     <label>Tahun Buku</label>
                     <input type="text" name="f" class="form-control" value="<?= $data['Thn'] ?>" placeholder="Enter Tahun Buku" required>
                  </div>
                  <div class="form-group">
                     <label>Sinopsis Buku</label>
                     <textarea type="text" name="g" class="form-control" placeholder="Enter Sinopsis Buku" required><?= $data['Sinopsis'] ?></textarea>
                  </div>
               </div>

               <div class="card-footer">
                  <button type="submit" name="ubah" class="btn btn-primary"> <i class="fa fa-save"></i>Ubah Data</button>
               </div>
            </form>

         </div>
      </div>
   </div>

<?php } ?>