<?php

include 'config/koneksi.php';
if (isset($_POST['simpan'])) {
   $kodebuku = $_POST['a'];
   $judul = $_POST['b'];
   $kt = $_POST['c'];
   $pengarang = $_POST['d'];
   $penerbit = $_POST['e'];
   $thn = $_POST['f'];
   $sinopsis = $_POST['g'];

   $sql = mysqli_query($koneksi, "INSERT INTO buku VALUES('$kodebuku','$judul','$kt','$pengarang','$penerbit','$thn','$sinopsis','default.jpg')");
   if ($sql) {
      echo '<script>
      setTimeout(function() {
          swal({
              title: "SUCCESS",
              text: "Record Berhasil Ditambahkan!",
              type: "success"
          }, function() {
              window.location = "index.php?page=view-buku";
          });
      }, 500);
  </script>';
   }
}

?>



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<div class="row">
   <div class="col-md-12">
      <div class="card shadow mb-4">
         <div class="card-header py-3 d-sm-flex align-items-center justify-content-between mb-4">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Data Buku</h6>
            <a href="index.php?page=view-buku" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-eye fa-sm text-white-50"></i> Tampil Data</a>
         </div>

         <form action="" method="POST">
            <div class="card-body col-md-6">
               <div class="form-group">
                  <label>Kode Buku</label>
                  <input type="text" name="a" class="form-control" placeholder="Enter Kode Buku" required>
               </div>
               <div class="form-group">
                  <label>Judul Buku</label>
                  <input type="text" name="b" class="form-control" placeholder="Enter Judul Buku" required>
               </div>
               <div class="form-group">
                  <label>Kategori Buku</label>
                  <select name="c" class="form-control">
                     <option selected disabled value="0">-Pilih Kategori-</option>
                     <?php
                     $dataKategori = mysqli_query($koneksi, "SELECT * FROM kategori");
                     while ($data = mysqli_fetch_array($dataKategori)) { ?>
                        <option value="<?= $data['IDKategori'] ?>"><?= $data['NmKategori'] ?></option>
                     <?php } ?>
                  </select>
               </div>
               <div class="form-group">
                  <label>Pengarang</label>
                  <input type="text" name="d" class="form-control" placeholder="Enter Pengarang" required>
               </div>
               <div class="form-group">
                  <label>Penerbit</label>
                  <input type="text" name="e" class="form-control" placeholder="Enter Penerbit" required>
               </div>
               <div class="form-group">
                  <label>Tahun Buku</label>
                  <input type="text" name="f" class="form-control" placeholder="Enter Tahun Buku" required>
               </div>
               <div class="form-group">
                  <label>Sinopsis Buku</label>
                  <textarea type="text" name="g" class="form-control" placeholder="Enter Sinopsis Buku" required></textarea>
               </div>
            </div>

            <div class="card-footer">
               <button type="submit" name="simpan" class="btn btn-primary"> <i class="fa fa-save"></i>Tambah Data</button>
            </div>
         </form>

      </div>
   </div>
</div>

<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>