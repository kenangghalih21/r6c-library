<?php

include 'config/koneksi.php';

$kd = $_GET['kd'];
$tampil = mysqli_query($koneksi, "SELECT * FROM buku LEFT JOIN kategori ON buku.Kategori = kategori.IDKategori WHERE buku.KodeBuku = '$kd'");
while ($data = mysqli_fetch_array($tampil)) { ?>

   <div class="row">
      <div class="col-md-12">
         <div class="card shadow mb-4">
            <div class="card-header py-3 d-sm-flex align-items-center justify-content-between mb-4">
               <h6 class="m-0 font-weight-bold text-primary">Detail Data Buku</h6>
               <a href="index.php?page=view-buku" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-eye fa-sm text-white-50"></i> Tampil Data</a>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="card-body">
                     <div class="form-group">
                        <label>Kode Buku</label><br>
                        <h4><?= $data['KodeBuku'] ?></h4>
                     </div>
                     <div class="form-group">
                        <label>Judul Buku</label>
                        <h5><?= $data['Judul'] ?></h5>
                     </div>
                     <div class="form-group">
                        <label>Kategori Buku</label>
                        <h5><?= $data['NmKategori'] ?></h5>
                     </div>
                     <div class="form-group">
                        <label>Pengarang</label>
                        <h5><?= $data['Pengarang'] ?></h5>
                     </div>
                     <div class="form-group">
                        <label>Penerbit</label>
                        <h5><?= $data['Penerbit'] ?></h5>
                     </div>
                     <div class="form-group">
                        <label>Tahun</label>
                        <h5><?= $data['Thn'] ?></h5>
                     </div>
                  </div>
               </div>
               <div class="card-body col-md-6">
                  <img src="upload_file/<?= $data['Gambar'] ?>" class="img-rounded" width="200px">
                  <hr class="border-primary">
                  <div class="form-group">
                     <label>Sinopsis Buku</label>
                     <h5><?= $data['Sinopsis'] ?></h5>
                  </div>
               </div>


            </div>
            <div class="card-footer">
               <button type="button" onclick="location.href='index.php?page=view-buku'" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Kembali </button>
            </div>
         </div>
      </div>
   </div>

<?php } ?>