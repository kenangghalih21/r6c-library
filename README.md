# Project Akhir PEMROGRAMAN WEB LANJUT R6C

## Final Project

## Tema Project : Library Data-book Helper

Motto: Organize books to help your work!

## R6C Library

Aplikasi Pengelolaan Data Buku Perpustakaan @Unindra

## ERD

<img src="/img/erd.jpg" width="550">

## Fitur Aplikasi

<ul>
   <li>Responsive Web</li>
   <li>Login Aplikasi</li>
   <li>CRUD (Create Update Delete)</li>
   <li>Upload File(Gambar)</li>
   <li>Upload/Cetak File -> PDF Reporting</li>
   <li>Ajax - javascript</li>
   <li>SweetAlert - js library</li>
</ul>

## View Halaman

<h3>Halaman Login </h3>
<img src="/img/login_.jpg" width="550">
<h3>Halaman Registrasi</h3>
<img src="/img/register_.jpg" width="550">
<h3>Tampilan Dashboard dgn Carousel</h3>
<img src="/img/dashboard.jpg" width="550">
<h3>Tampilan List Data Buku - CRUD</h3>
<img src="/img/dashboard2.jpg" width="550">
<h3>Fitur SweetAlert</h3>
<img src="/img/dashboard3.jpg" width="550">
<h3>Gallery</h3>
<img src="/img/gallery.jpg" width="550">
<p> Responsive Web Application based on PHP, MySql and made by CSS Framework: Bootstrap with sbadmin-2 as a template. <a href="https://startbootstrap.com/theme/sb-admin-2" target="_blank">Link Template sbadmin2</a>
<br>
Aplikasi ini dibuat untuk menyelesaikan tugas akhir PWL kelompok kami yang beranggotakan, sbb: </p>

## Anggota Kelompok

<img src="/img/members.jpg" width="550">

<ol>
   <li>Kenang Ghalih Diasthama - 201943500208</li>
   <li>Asmi Salam Ramadhani - 201943500239</li>
   <li>Bella Permata Putri - 201943500236</li>
   <li>Syifa Salsabila - 201943500186</li>
</ol>

## Penjelasan dan Demo Aplikasi

<a href="https://youtu.be/35c0MKafgQQ" target="_blank">Klik Untuk Membuka Video</a>
