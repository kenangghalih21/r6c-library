<?php
//panggil koneksi
include 'config/koneksi.php';
session_start();
if (isset($_POST['login'])) {
    // simpan nilai inoutan pd variabel
    $user =  $_POST['username'];
    $pass =  md5($_POST['password']);
    // cek pd query nilai input dan nilai pd tabel
    $sql =  mysqli_query($koneksi, "SELECT * FROM login WHERE Username = '$user' AND Password = '$pass'");
    $data = mysqli_fetch_array($sql);
    // cek jumlah data
    if (mysqli_num_rows($sql) > 0) {
        // simpan nilai pd sesi
        $_SESSION['user'] = $user;
        $_SESSION['status'] = "login";
        $_SESSION['nama'] = $data['Name'];
        // setelah aray sesi disimpan, arahkan ke index.php
        echo '<script>
                setTimeout(function() {
                    swal({
                        title: "SUCCESS",
                        text: "Berhasil Login!",
                        type: "success"
                    }, function() {
                        window.location = "index.php";
                    });
                }, 500);
            </script>';
    } else {
        header("location:login.php?status=gagal");
    }
}

?>





<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/buku.png" type="image/ico">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

    <style>
        body {
            background: #007bff;
            background: linear-gradient(to right, #0062E6, #33AEFF);
        }
    </style>
    <title>Login | R6C Library</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-12 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block">
                                <img src="img/login.jpg" alt="">
                            </div>
                            <div class=" col-lg-6 border-left-primary">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4"> <b> LOGIN SYSTEM</b><br> <b class="text-primary">R6C LIBRARY</b></h1>
                                    </div>
                                    <?php
                                    if (isset($_GET['status'])) {
                                        if ($_GET['status'] == 'logindulu') {
                                            echo "<div class='alert alert-warning'>Maaf! Silahkan Login Terlebih Dahulu</div>";
                                        } elseif ($_GET['status'] == 'gagal') {
                                            echo "<div class='alert alert-danger'>Username / Password Tidak Sesuai!</div>";
                                        } elseif ($_GET['status'] == 'logout') {
                                            echo "<div class='alert alert-success'> Berhasil Logout</div>";
                                        }
                                    }
                                    ?>
                                    <form class="user" action="" method="POST">
                                        <div class="form-group">
                                            <input type="text" name="username" class="form-control form-control-user" placeholder=" Username" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                        </div>
                                        <button name="login" class="btn btn-primary btn-user btn-block">
                                            <b>LOGIN</b>
                                        </button>
                                    </form>

                                    <hr>
                                    <div class="text-left">
                                        <p>Belum Punya Akun?
                                            <u><a class="font-weight-normal" href="registrasi.php">Buat Akun Baru</a></u>
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    <!-- s -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

</body>

</html>