<div class="row">
   <div class="col-md-9">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
         </ol>
         <div class="carousel-inner">
            <div class="carousel-item active">
               <img class="d-block w-100" src="img/cover1.jpg" alt="First slide">
               <div class="carousel-caption d-inline p-3 bg-primary text-white">
                  <h3>WELCOME <?= $_SESSION['nama'] ?></h3>
                  <h5>Selamat Datang Di Aplikasi R6C Library!</h5>
               </div>
            </div>
            <div class="carousel-item">
               <img class="d-block w-100" src="img/cover2.jpg" alt="Second slide">
               <div class="carousel-caption d-inline p-2 bg-white text-dark">
                  <h5>Aplikasi R6C Library merupakan aplikasi yang diciptakan oleh tim developer R6C untuk mengelola data buku yang ada di perpustakaan Unindra!</h5>
               </div>
            </div>

         </div>
         <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
         </a>
      </div>
   </div>
   <div class="col-md-3 border-left-primary">
      <div class="card-body">
         <h3 class="text-primary">R6C LIBRARY</h3>
         <p>Responsive Web Application based on PHP, MySql and made by CSS Framework: Bootstrap with sbadmin-2 as a template.</p>
         <a href="https://gitlab.com/kenangghalih21/r6c-library" target="_blank">Our commit on gitlab</a>
         <hr class="border-primary">
         <h3 class="text-primary">Our Features:</h3>
         <ul>
            <li>Responsive Web</li>
            <li>Login Application</li>
            <li>CRUD</li>
            <li>Upload Image</li>
            <li>PDF Reporting</li>
            <li>Ajax - javascript</li>
            <li>SweetAlert</li>
         </ul>
      </div>
   </div>
</div>